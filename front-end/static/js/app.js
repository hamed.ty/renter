var app = angular.module("app", []);

app.controller("ctrl", function ctrl($scope) {
  $scope.maxRent = 350;

  $scope.init = function() {
    var map = L.map("map").setView([35.7, 51.4], 11);
    L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png").addTo(
      map
    );

    var markers = L.markerClusterGroup();
    markers.on("click", function(a) {
      var scope = angular.element(document.querySelector("[ng-app]")).scope();
      scope.$apply(function() {
        scope.selected = a.layer.data;
      });
    });
    map.addLayer(markers);
    $scope.markers = markers;
  };
  $scope.priceChange = function() {
    fetch("/static/data/summary.json")
      .then(response => response.json())
      .then(data => {
        $scope.markers.clearLayers();
        data.forEach(d => {
          price = d[3] + (d[4] / 3.0) * 100;
          if (price > $scope.maxRent) return;
          marker = L.marker([d[1], d[2]]);
          marker.data = d;
          $scope.markers.addLayer(marker);
        });
      });
  };
  $scope.init();
  $scope.priceChange();
});

app.filter("ngPersian", function() {
  return function(number) {
    if (!number) return;
    var englishNumbers = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"];
    var arabicNumbers = ["۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹", "۰"];

    var numberToString = number.toString();

    for (var i = 0; i < englishNumbers.length; i++) {
      numberToString = numberToString.replace(
        new RegExp(englishNumbers[i], "g"),
        arabicNumbers[i]
      );
    }

    return numberToString;
  };
});
