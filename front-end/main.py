import os
import time
import json
import cherrypy

SERVER_PATH = os.path.dirname(os.path.realpath(__file__))
BASE_PATH = os.path.dirname(SERVER_PATH)
STATIC_PATH = os.path.join(SERVER_PATH, 'static')
FAVICON_PATH = os.path.join(STATIC_PATH, 'favicon.ico')

DATA = json.loads(open('static/data/summary.json').read())


class Server(object):
    @cherrypy.expose
    def index(self):
        raise cherrypy.HTTPRedirect("/static/html/index.html")

    @cherrypy.expose
    @cherrypy.tools.json_in()
    @cherrypy.tools.json_out()
    def api(self):
        return DATA

    @cherrypy.expose
    def device(self):
        return 'salam'


def run_server():
    config = {
        '/': {
            'tools.gzip.on': True,
        },
        '/static': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': STATIC_PATH,
        },
        'global': {
            # 'log.screen': False,
            'engine.autoreload.on': False,
            'server.socket_host': '0.0.0.0',
            'server.socket_port': 8080,
        },
        '/favicon.ico':
            {
                'tools.staticfile.on': True,
                'tools.staticfile.filename': FAVICON_PATH,
        }

    }
    # def browse():
    #     webbrowser.open("http://127.0.0.1:8080")
    # cherrypy.engine.subscribe('start', browse, priority=90)
    cherrypy.quickstart(Server(), '/', config=config)


if __name__ == "__main__":
    run_server()
