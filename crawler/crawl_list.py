import requests
import json
import glob


def fetch_date(last_date):
    url = 'https://api.divar.ir/v8/search/1/real-estate'
    data = {"json_schema": {"category": {"value": "real-estate"}},
            "last-post-date": last_date}
    r = requests.post(url, json=data)
    assert r.status_code == 200
    data = r.json()
    with open('raw_data/lists/%d.json' % last_date, 'w') as f:
        f.write(json.dumps(data, indent=4, sort_keys=True))
    print(last_date, len(data['widget_list']),
          data['widget_list'][-1]['data']['normal_text'])
    return data['last_post_date']


def extract_first_date():
    url = 'https://divar.ir/s/tehran/rent-residential'
    r = requests.get(url)
    lines = [l.strip() for l in r.text.splitlines()]
    _, data = [l for l in lines if l.startswith(
        'window.__PRELOADED_STATE__')][0].split('=', 1)

    data = json.loads(json.loads(data[:-1]))
    date = data['browse']['linkedDataRequirements']['lastPostDate']
    return date


def extract_last_date():
    ls = glob.glob('raw_data/lists/*.json')
    ls.sort()
    with open(ls[0]) as f:
        last_date = json.loads(f.read())['last_post_date']
    return last_date


def main():
    last_date = extract_first_date()
    # last_date = extract_last_date()

    while True:
        last_date = fetch_date(last_date)


if __name__ == '__main__':
    main()
